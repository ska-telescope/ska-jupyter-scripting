FROM artefact.skao.int/ska-tango-images-pytango-builder:9.4.3
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/install.python-poetry.org/main/install-poetry.py | python3 - --yes --version 1.8.2

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

ARG USER=newuser
ENV USER ${USER}
ENV HOME /home/${USER}
ENV PATH /home/${USER}/.local/bin:${PATH}

RUN userdel tango
RUN useradd --create-home --home-dir /home/${USER} ${USER}
RUN usermod -u 1000 -g 1000 ${USER}

WORKDIR ${HOME}

COPY --chown=${USER}:${USER} . ./

USER ${USER}

COPY pyproject.toml poetry.lock ./

RUN poetry export --format requirements.txt --output poetry-requirements.txt --without-hashes && \
    sed -i '/pytango/d' poetry-requirements.txt && \
    sed -i '/numpy/d' poetry-requirements.txt && \
    pip install -r poetry-requirements.txt && \
    rm poetry-requirements.txt 

RUN mkdir data

RUN curl https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-core/-/raw/main/data/AA05LOW.ms.tar.gz --output data/AA05LOW.ms.tar.gz

RUN cd data/ && tar -xzf AA05LOW.ms.tar.gz && cd ..
