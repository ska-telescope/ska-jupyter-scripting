# pylint: disable=no-name-in-module, no-member

"""
Utils for running the Mid 5-point calibration scan notebook
"""
import logging

import aiokafka
import h5py
import numpy
from astropy.time import Time, TimeDelta
from ska_sdp_cbf_emulator.packetiser import (
    MeasurementSetDataSourceConfig,
    SenderConfig,
    packetise,
)
from ska_sdp_cbf_emulator.transmitters.spead2_transmitters import (
    Spead2TransmissionConfig,
    TransportProtocol,
)

logger = logging.getLogger(__name__)

# Set of configurable variables for vis receive
TOTAL_CHANNELS = 32  # should match emulator data
TOTAL_STREAMS = 1  # total number of spead streams to use
# number of times the emulator will repeat the scan data on disk
NUM_REPEATS = 1

POINTING_DTYPE = [
    ("antenna_id", "int"),
    ("ts", "datetime64[s]"),
    ("az", "float64"),
    ("el", "float64"),
]

_MJD_TO_Y2000TAI_SEC: float = (
    Time("2000-01-01", format="isot", scale="tai") - Time(0.0, format="mjd")
).sec


async def cbf_scan(
    ms_path: str, target_host: str, target_port: str, scan_id: int
):
    """
    Run the CBF emulator

    :param ms_path: path to MeasurementSet to be sent
    :param target_host: host address of vis-receive pod
    :param target_port: port of vis-receive pod
    :param scan_id: ID of the scan that is running
    """
    sender_config = SenderConfig(scan_id=scan_id, time_interval=-1)

    sender_config.ms = MeasurementSetDataSourceConfig(
        location=ms_path, num_repeats=NUM_REPEATS
    )

    sender_config.transmission = Spead2TransmissionConfig(
        target_host=target_host,
        target_port_start=target_port,
        transport_protocol=TransportProtocol.TCP,
        rate=0,
        num_streams=TOTAL_STREAMS,
    )

    await packetise(sender_config)


async def mock_pointings(dish_ids, data_file, kafka_topic, kafka_host):
    """
    Send pointing data to be written into the MeasurementSet
    before processing.

    :param dish_ids: list if IDs for "mock dishes"
    :param data_file: path to pointing HDF file to be sent for given scan
    :param kafka_topic: Kafka topic to send data to
    :param kafka_host: Kafka host address
    """
    # epoch to convert pointing timestamp to timestamp expected by the kafka producer
    epoch = Time("1970-01-01", format="isot", scale="utc").isot
    epoch_dt = numpy.datetime64(epoch)

    async with aiokafka.AIOKafkaProducer(
        bootstrap_servers=kafka_host
    ) as producer:
        for dish_id in dish_ids:
            for pointing_data in stream_hdf5_pointings(
                h5py.File(data_file), dish_id
            ):
                timestamp = (
                    Time("2000-01-01", format="isot", scale="tai")
                    + TimeDelta(pointing_data[0], format="sec")
                ).isot
                datetime = numpy.datetime64(timestamp)

                timestamp_ms = numpy.timedelta64(datetime - epoch_dt, "ms")

                pointing = numpy.array(
                    [(dish_id, datetime, pointing_data[1], pointing_data[2])],
                    dtype=POINTING_DTYPE,
                )
                await producer.send_and_wait(
                    f"{kafka_topic}_{dish_id}",
                    pointing.tobytes(),
                    timestamp_ms=timestamp_ms.astype(int),
                )


def stream_hdf5_pointings(file: h5py.File, antenna_id: int):
    """Creates a generator to time ordered pointing values read from HDF5 file validated by
    `assert_hdf5_pointing_table`.

    Args:
        file (h5py.File): HDF file of pointing table(s).
        antenna_id (int): antenna_id index within HDF file.

    Yields:
        Generator[np.ndarray, None, None]: pointing generator of [time(s), az, el] ndarrays.
    """
    # sort group keys by timestamp if using multiple tables
    keys = list(file.keys())
    keys.sort(key=lambda k: file[k]["data_time"][0])  # type: ignore
    for key in keys:
        group = file[key]
        for time_idx, data_time_mjds in enumerate(group["data_time"]):
            # coords: time, antenna, frequency, receptor, angle
            pointing_ds: h5py.Dataset = group["data_pointing"]  # type: ignore
            azel: numpy.ndarray = pointing_ds[time_idx, antenna_id, :, :, :]
            azel_ave = numpy.average(azel, axis=(0, 1))
            desired_pointing = numpy.array(
                [
                    data_time_mjds - _MJD_TO_Y2000TAI_SEC,
                    azel_ave[0],
                    azel_ave[1],
                ]
            )
            yield desired_pointing


def qametric_exchange(subarray_id, kafka_host):
    """
    Generate QueueConnector exchange for QA metrics
    """
    qametric_exch = {
        "dtype": "object",
        "shape": [],
        "source": {
            "type": "KafkaConsumerSource",
            "servers": kafka_host,
            "topic": f"metrics-receive_state-{subarray_id}",
            "encoding": "json",
        },
        "sink": {
            "type": "TangoObjectScatterAttributeSink",
            "attributes": [
                {
                    "attribute_name": "receiver_state",
                    "filter": "type=='visibility_receive'",
                    "path": "state",
                    "dtype": "str",
                    "default_value": "unknown",
                },
                {
                    "attribute_name": "last_update",
                    "filter": "type=='visibility_receive'",
                    "path": "time",
                    "dtype": "float",
                    "default_value": 0.0,
                },
                {
                    "attribute_name": "processing_block_id",
                    "filter": "type=='visibility_receive'",
                    "path": "processing_block_id",
                    "dtype": "str",
                    "default_value": "",
                },
                {
                    "attribute_name": "execution_block_id",
                    "filter": "type=='visibility_receive'",
                    "path": "execution_block_id",
                    "dtype": "str",
                    "default_value": "",
                },
                {
                    "attribute_name": "subarray_id",
                    "filter": "type=='visibility_receive'",
                    "path": "subarray_id",
                    "dtype": "str",
                    "default_value": "-1",
                },
                {
                    "attribute_name": "scan_id",
                    "filter": "type=='visibility_receive'",
                    "path": "scan_id",
                    "dtype": "int",
                    "default_value": 0,
                },
                {
                    "attribute_name": "payloads_received",
                    "filter": "type=='visibility_receive'",
                    "path": "payloads_received",
                    "dtype": "int",
                    "default_value": 0,
                },
                {
                    "attribute_name": "time_since_last_payload",
                    "filter": "type=='visibility_receive'",
                    "path": "time_since_last_payload",
                    "dtype": "float",
                    "default_value": 0.0,
                },
            ],
        },
    }
    return qametric_exch


def mock_dish_exchange(dish_ids, kafka_host):
    """
    Generate QueueConnector exchange for Mock Dishes
    """
    master_dish_exchange = [
        {
            # Mock data into Tango array attribute via kafka
            # (cannot write bytestring to json)
            "dtype": POINTING_DTYPE,
            "shape": [1],
            "source": {
                "type": "KafkaConsumerSource",
                "topic": f"achieved_pointing_{d}",
                "servers": kafka_host,
                "encoding": "carray",
            },
            "sink": {
                "type": "KafkaProducerSink",
                "servers": kafka_host,
                "topic": "actual-pointings",
                "encoding": "npy",
            },
        }
        for d in dish_ids
    ]

    leaf_dish_exchange = [
        {
            # Mock data into Tango array attribute via kafka
            "dtype": POINTING_DTYPE,
            "shape": [1],
            "source": {
                "type": "KafkaConsumerSource",
                "topic": f"desired_pointing_{d}",
                "servers": kafka_host,
                "encoding": "carray",
            },
            "sink": {
                "type": "KafkaProducerSink",
                "servers": kafka_host,
                "topic": "commanded-pointings",
                "encoding": "npy",
            },
        }
        for d in dish_ids
    ]
    return master_dish_exchange + leaf_dish_exchange


def mswriter_processor():
    """
    Generate vis-receive processor for mswriter with pointings.
    """
    processor = {
        "mswriter": {
            "args": [
                "--readiness-file=/tmp/processor_ready",
                "--commanded-pointing-topic=commanded-pointings",
                "--actual-pointing-topic=actual-pointings",
                "output.ms",
            ],
        }
    }
    return processor
