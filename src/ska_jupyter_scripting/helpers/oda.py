import os

from ska_oso_pdm import SBDefinition
from ska_oso_pdm.openapi import CODEC
from ska_oso_scripting import oda_helper


def add_sbs_to_oda(filepath: str = "../sb"):
    filepath = os.path.abspath(filepath)
    for filename in os.listdir(filepath):
        if filename.endswith(".json"):
            file_with_path = os.path.join(filepath, filename)
            sb = CODEC.load_from_file(SBDefinition, file_with_path)
            sb.sbd_id = None
            saved_sb = oda_helper.save(sb)
            print(f"Adding {filename} to ODA with ID {saved_sb.sbd_id}")
