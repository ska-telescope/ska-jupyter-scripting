import matplotlib.pyplot as plt
import numpy as np
from casacore.tables import table


def load_measurement_set(ms_file):
    """
    Load measurement set and extract necessary data.
    """
    with table(ms_file + "/SPECTRAL_WINDOW") as spw:
        chan_freqs = spw.getcol("CHAN_FREQ")[0]

    with table(ms_file) as ms:
        antenna1 = ms.getcol("ANTENNA1")
        antenna2 = ms.getcol("ANTENNA2")
        visibilities = ms.getcol("DATA")
        time_data = ms.getcol("TIME")

    unique_times = np.unique(time_data)

    return (
        chan_freqs,
        unique_times,
        visibilities,
        antenna1,
        antenna2,
        time_data,
    )


def extract_uv_coverage_weight_data(ms_file):
    """
    Extract data necessary for uv coverage and weight distribution plots
    """

    with table(ms_file + "/SPECTRAL_WINDOW") as spw:
        chan_freqs = spw.getcol("CHAN_FREQ")[0]

    with table(ms_file) as ms:
        uvw_data = ms.getcol("UVW")
        weights = ms.getcol("WEIGHT")
        time_data = ms.getcol("TIME")

    return (chan_freqs, uvw_data, weights, time_data)


def process_uv_coverage_weight_data(
    weights, chan_freqs, uvws, time_data, max_time
):

    time_mask = time_data <= max_time
    filtered_uvws = uvws[time_mask]
    filtered_weights = weights[time_mask]

    min_freq = chan_freqs.min()
    max_freq = chan_freqs.max()
    mid_frequency = (min_freq + max_freq) / 2
    wavelength = 299792458 / mid_frequency

    u_values = np.round(filtered_uvws[:, 0] / wavelength, 5)
    v_values = np.round(filtered_uvws[:, 1] / wavelength, 5)

    return (u_values, v_values, filtered_weights)


def plot_uv_coverage_weight_distribution(
    time_index, weights, chan_freqs, uvws, time_data
):

    max_time = np.unique(time_data)[time_index]

    u_values, v_values, weights = process_uv_coverage_weight_data(
        weights, chan_freqs, uvws, time_data, max_time
    )

    nrows, ncols = (2, 2)

    fig, axes = plt.subplots(nrows, ncols, figsize=(10, 6), sharex=True)
    pol_labels = ["XX", "XY", "YX", "YY"]
    for pol in range(len(pol_labels)):
        ax = axes.flat[pol]
        ax.clear()
        ax.set_title(f"Polarisation {pol_labels[pol]}")
        ax.scatter(
            u_values,
            v_values,
            label=f"Polarisation {pol}",
            c=weights[:, pol],
        )
        ax.scatter(-u_values, -v_values, c=weights[:, pol])
        ax.set_xlabel(r"u (N$\lambda$)")
        ax.set_ylabel(r"v (N$\lambda$)")

    plt.suptitle(f"uv coverage/weight distribution at Time {max_time: .3f} s")
    plt.tight_layout()
    plt.show()


def process_spectrum_for_time(
    chan_freqs,
    visibilities,
    antenna1,
    antenna2,
    time_data,
    target_time,
    nchan_avg,
):
    """
    Extracts and processes spectrum data for a specific timestamp.
    """

    # Select only autocorrelations
    auto_mask = antenna1 == antenna2

    time_mask = time_data == target_time
    selected_mask = auto_mask & time_mask
    selected_vis = visibilities[selected_mask]

    # Apply channel averaging
    if nchan_avg > 1:
        num_rows, num_channels, num_pols = selected_vis.shape
        padded_size = (
            num_channels + (nchan_avg - num_channels % nchan_avg) % nchan_avg
        )

        padded_data = np.pad(
            selected_vis,
            ((0, 0), (0, padded_size - num_channels), (0, 0)),
            mode="constant",
            constant_values=np.NaN,
        )
        reshaped_data = padded_data.reshape(num_rows, -1, nchan_avg, num_pols)
        selected_vis = np.nanmean(reshaped_data, axis=2)

        padded_freqs = np.pad(
            chan_freqs, (0, padded_size - num_channels), mode="edge"
        )
        reshaped_freqs = padded_freqs.reshape(-1, nchan_avg)
        chan_freqs = np.mean(reshaped_freqs, axis=1)

    selected_vis = selected_vis[:, :, :]
    spectrum_mean = np.mean(selected_vis, axis=0)
    magnitude = np.abs(spectrum_mean)

    spectrum_data = {
        "channel_frequencies": chan_freqs / 1e6,  # Convert to MHz
        "magnitude": magnitude,
    }

    return spectrum_data


def update_spectrum_plot(
    time_index,
    unique_times,
    chan_freqs,
    visibilities,
    antenna1,
    antenna2,
    time_data,
    nchan_avg,
):
    """
    Time index slicing for updating a spectrum plot.
    """
    target_time = unique_times[time_index]
    spectrum_data = process_spectrum_for_time(
        chan_freqs,
        visibilities,
        antenna1,
        antenna2,
        time_data,
        target_time,
        nchan_avg,
    )

    if spectrum_data is None:
        print(f"No data found for time {target_time: .3f} s")
        return

    freqs = spectrum_data["channel_frequencies"]
    num_pols = spectrum_data["magnitude"].shape[1]

    nrows, ncols = (2, 2)

    fig, axes = plt.subplots(nrows, ncols, figsize=(10, 6), sharex=True)
    pol_labels = ["XX", "XY", "YX", "YY"]
    for pol in range(num_pols):
        ax = axes.flat[pol]
        ax.clear()
        ax.set_title(f"Polarisation {pol_labels[pol]}")
        ax.step(
            freqs,
            spectrum_data["magnitude"][:, pol],
            label=f"Polarisation {pol}",
            color="tab:blue",
            where="mid",
        )
        ax.set_ylabel("Magnitude (log)")
        ax.set_yscale("log")
        ax.legend()

    axes.flat[-1].set_xlabel("Frequency (MHz)")
    axes.flat[-2].set_xlabel("Frequency (MHz)")
    plt.suptitle(f"Spectrum at Time {target_time: .3f} s")
    plt.tight_layout()
    plt.show()


def generate_data(data, times, antenna1, antenna2, nchan_avg, chan_freqs):
    """
    Compute the phase and magnitude spectrum from a Measurement Set.
    """

    # Extract unique baselines and polarisations
    baselines = np.unique(np.vstack((antenna1, antenna2)).T, axis=0)
    polarisations = data.shape[-1]

    unique_times = np.unique(times)

    results = []
    for time in unique_times:
        time_mask = times == time
        selected_data = data[time_mask]
        results.append(
            _process_data(
                selected_data,
                baselines,
                polarisations,
                nchan_avg,
                chan_freqs,
                time,
            )
        )

    return results


def _process_data(
    data: np.ndarray,
    baselines: np.ndarray,
    polarisations: int,
    nchan_avg: int,
    chan_freqs: np.ndarray,
    time: float,
):
    """
    Process visibility data to compute phase and magnitude values per baseline.
    """

    if nchan_avg > 1:
        num_baselines, num_channels, num_polarisations = data.shape
        padded_size = (
            num_channels + (nchan_avg - num_channels % nchan_avg) % nchan_avg
        )

        padded_data = np.pad(
            data.astype(complex),
            ((0, 0), (0, padded_size - num_channels), (0, 0)),
            mode="constant",
            constant_values=np.NaN,
        )

        reshaped_data = padded_data.reshape(
            num_baselines, -1, nchan_avg, num_polarisations
        )
        data = np.nanmean(reshaped_data, axis=2)

        padded_freqs = np.pad(
            chan_freqs, (0, padded_size - num_channels), mode="edge"
        )
        reshaped_freqs = padded_freqs.reshape(-1, nchan_avg)
        chan_freqs = np.mean(reshaped_freqs, axis=1)

    angles = np.round(np.angle(data[:, :, :]), 5)
    magnitude = np.round(np.abs(data[:, :, :]), 5)

    payload = [
        {
            "baseline": tuple(baseline),
            "polarisation": pol_index,
            "time": time,
            "chan_freqs": chan_freqs,
            "phase": angles[b_idx, :, pol_index].tolist(),
            "magnitude": magnitude[b_idx, :, pol_index].tolist(),
            "vis": data[b_idx, :, pol_index],
        }
        for b_idx, baseline in enumerate(baselines)
        for pol_index in range(polarisations)
    ]

    return payload


def update_phase_plot(phase_data, time_index):
    """
    Plots phase spectra with a slider to step through time indices.
    """

    first_timestep_data = phase_data[0]
    unique_baselines = sorted(
        set((d["baseline"] for d in first_timestep_data))
    )

    fig, axes = plt.subplots(2, 2, figsize=(12, 10), sharex=True, sharey=True)
    pol_labels = ["XX", "XY", "YX", "YY"]
    payloads = phase_data[time_index]

    for pol_index in range(4):
        ax = axes[pol_index // 2, pol_index % 2]

        for baseline in unique_baselines:
            matching_data = [
                d
                for d in payloads
                if d["baseline"] == baseline and d["polarisation"] == pol_index
            ]

            if matching_data:
                freqs = (
                    np.array([d["chan_freqs"] for d in matching_data]).T / 1e6
                )
                phases = np.array([d["phase"] for d in matching_data])

                ax.plot(
                    freqs,
                    phases.T,
                    linestyle="-",
                    label=f"Baseline {baseline}",
                )

        ax.set_title(f"Polarisation {pol_labels[pol_index]}")
        ax.set_xlabel("Frequency (MHz)")
        ax.set_ylabel("Phase (radians)")
        ax.grid()
        ax.legend(loc="best", fontsize=8)

    plt.suptitle(
        f"Phase Spectrum for All Baselines, Time Index {time_index}",
        fontsize=14,
    )
    plt.tight_layout(rect=[0, 0, 1, 0.96])
    plt.show()


def update_magnitude_plot(magnitude_data, time_index):
    """
    Plots phase spectra with a slider to step through time indices.
    """
    first_timestep_data = magnitude_data[0]
    unique_baselines = sorted(
        set((d["baseline"] for d in first_timestep_data))
    )

    fig, axes = plt.subplots(2, 2, figsize=(12, 10), sharex=True, sharey=True)
    pol_labels = ["XX", "XY", "YX", "YY"]
    payloads = magnitude_data[time_index]

    for pol_index in range(4):
        ax = axes[pol_index // 2, pol_index % 2]

        for baseline in unique_baselines:
            matching_data = [
                d
                for d in payloads
                if d["baseline"] == baseline and d["polarisation"] == pol_index
            ]

            if matching_data:
                freqs = (
                    np.array([d["chan_freqs"] for d in matching_data]).T / 1e6
                )
                mags = np.array([d["magnitude"] for d in matching_data])
                ax.plot(
                    freqs, mags.T, linestyle="-", label=f"Baseline {baseline}"
                )

        ax.set_title(f"Polarisation {pol_labels[pol_index]}")
        ax.set_xlabel("Frequency (MHz)")
        ax.set_ylabel("Magnitude")
        ax.set_yscale("log")
        ax.grid()
        ax.legend(loc="best", fontsize=8)

    plt.suptitle(
        f"Magnitude for All Baselines, Time Index {time_index}", fontsize=14
    )
    plt.tight_layout(rect=[0, 0, 1, 0.96])
    plt.show()


def plot_spectrograms(phase_data):
    """
    Generate a grid of heatmaps showing phase vs time for each baseline and polarisation.
    """
    baselines = sorted(
        set((entry["baseline"] for result in phase_data for entry in result))
    )
    polarisations = sorted(
        set(entry["polarisation"] for result in phase_data for entry in result)
    )
    times = sorted(
        set(entry["time"] for result in phase_data for entry in result)
    )

    rows = len(baselines)
    cols = len(polarisations)
    fig, axes = plt.subplots(rows, cols, figsize=(4 * cols, 3 * rows))

    for r, baseline in enumerate(baselines):
        for c, pol in enumerate(polarisations):
            phase_matrix = []
            chan_freqs = None

            for time in times:
                for result in phase_data:
                    entry = next(
                        (
                            e
                            for e in result
                            if e["baseline"] == baseline
                            and e["polarisation"] == pol
                            and e["time"] == time
                        ),
                        None,
                    )
                    if entry:
                        phase_matrix.append(entry["phase"])
                        if chan_freqs is None:
                            chan_freqs = entry["chan_freqs"]

            phase_matrix = np.array(phase_matrix)

            ax = axes[r, c]
            im = ax.imshow(
                phase_matrix,
                aspect="auto",
                cmap="viridis",
                vmin=-np.pi,
                vmax=np.pi,
                extent=[chan_freqs[0], chan_freqs[-1], times[-1], times[0]],
                interpolation="none",
            )

            ax.set_title(f"Baseline {baseline}, Pol {pol}")
            if r == rows - 1:
                ax.set_xlabel("Frequency (Hz)")
            if c == 0:
                ax.set_ylabel("Time")

            if c == 3:
                cbar = fig.colorbar(im, ax=ax)
                cbar.set_label("Phase (rad)")

    plt.tight_layout()
    plt.show()


def plot_lagplots(data):
    """
    Generate a grid of heatmaps showing cross correlation power vs time lag for each baseline and polarisation.
    """
    baselines = sorted(
        set((entry["baseline"] for result in data for entry in result))
    )
    polarisations = sorted(
        set(entry["polarisation"] for result in data for entry in result)
    )
    times = sorted(set(entry["time"] for result in data for entry in result))

    rows = len(baselines)
    cols = len(polarisations)
    fig, axes = plt.subplots(rows, cols, figsize=(4 * cols, 3 * rows))

    chans = data[0][0]["chan_freqs"]
    f_width = chans.max() - chans.min() / chans.shape[0]
    half_count = chans.shape[0] / 2
    lags = np.array(
        [(i - half_count) * (1 / f_width) for i in range(chans.shape[0])]
    )

    for r, baseline in enumerate(baselines):
        for c, pol in enumerate(polarisations):
            phase_matrix = []

            for time in times:
                for result in data:
                    entry = next(
                        (
                            e
                            for e in result
                            if e["baseline"] == baseline
                            and e["polarisation"] == pol
                            and e["time"] == time
                        ),
                        None,
                    )
                    if entry:
                        vis = np.array(entry["vis"])
                        fft_data = np.abs(
                            np.fft.fftshift(np.fft.ifft(vis, axis=0), axes=0)
                        )
                        fft_data /= np.max(fft_data, axis=0, keepdims=True)
                        fft_data *= 360
                        phase_matrix.append(fft_data)

            phase_matrix = np.array(phase_matrix)

            ax = axes[r, c]
            im = ax.imshow(
                phase_matrix,
                aspect="auto",
                cmap="viridis",
                extent=[lags[0], lags[-1], times[-1], times[0]],
                interpolation="none",
            )

            ax.set_title(f"Baseline {baseline}, Pol {pol}")
            if r == rows - 1:
                ax.set_xlabel("Frequency (Hz)")
            if c == 0:
                ax.set_ylabel("Time")

            if c == 3:
                cbar = fig.colorbar(im, ax=ax)
                cbar.set_label("Power")

    plt.tight_layout()
    plt.show()


def plot_band_av_x_corr(data):
    """
    Plot the band averaged cross-correlation power, per baseline and polarisation.
    """
    baselines = sorted(
        set((entry["baseline"] for result in data for entry in result))
    )
    polarisations = sorted(
        set(entry["polarisation"] for result in data for entry in result)
    )
    times = sorted(set(entry["time"] for result in data for entry in result))

    fig, axes = plt.subplots(2, 2, figsize=(8, 6))
    pol_labels = ["XX", "XY", "YX", "YY"]

    for r, baseline in enumerate(baselines):
        for c, pol in enumerate(polarisations):
            ax = axes[c // 2, c % 2]
            powers = []

            for time in times:
                for result in data:
                    entry = next(
                        (
                            e
                            for e in result
                            if e["baseline"] == baseline
                            and e["polarisation"] == pol
                            and e["time"] == time
                        ),
                        None,
                    )
                    if entry:
                        power = np.abs(
                            np.square(np.mean(np.array(entry["vis"])))
                        )
                        powers.append(power)

            ax.set_yscale("log")
            ax.set_ylabel("Power (log)")
            ax.set_xlabel("Time (s)")
            ax.set_title(f"Pol: {pol_labels[pol]}")
            ax.plot(times, powers, label=baseline)

    plt.tight_layout()
    axes[1, 1].legend(loc="center right", bbox_to_anchor=(1.5, 0.5))
    plt.show()
