>>>
<h2>Health Warning:</h2> This repo is undergoing MAJOR changes in PI23 (Sprint 1 and Sprint 2, particularly). 100% functionality is NOT GUARANTEED during this time. This warning will be removed when stability is restored. Thank you for your patience.
<h1>STATUS:</h1> LOW `Chart.yaml` and `values.yaml` have been updated to the latest versions and have been shown to successfully complete the Assign-Configure-Scan-EndScan-GoToIdle-ReleaseResource when controlled via the CSP.LMC. TMC, SDP and MCCS not yet tested.
>>>


# ska-jupyter-scripting
## Introduction
This project contains a set of template Jupyter Notebooks that can be deployed and executed in the [SKAO BinderHub](https://k8s.stfc.skao.int/binderhub/). (Documentation on BinderHub can be found [here](https://developer.skao.int/en/latest/tools/binderhub.html)).
The goal of the Notebooks is to exercise the current capabilities of the SKAO LOW/MID software suite, allowing a set of 'walk-throughs' of AA0.5 MID/LOW Observing Scenarios in purely software environments.

### List of template walkthrough Notebooks available
1. [MID Interferometric Imaging (both single and multiple sources)](https://confluence.skatelescope.org/display/UD/MID+Imaging+Scenario+Walkthrough+Jupyter+Notebook) <span style="color:blue">Warning: Currently out of date naming convention in text</span>
1. [LOW Interferometric Imaging (both single and multiple sources)](https://confluence.skatelescope.org/display/UD/LOW+Imaging+Scenario+Walkthrough+Jupyter+Notebook) <span style="color:blue">Warning: Currently out of date naming convention in text</span>
1. MID 5-point reference calibration scan controls <TBD>
1. MID 5-point reference calibration scan data processing <TBD>

---

## For Users of the Software
To use the Notebooks present in this repository you will need to
1. Access to [SKAO BinderHub](https://k8s.stfc.skao.int/binderhub/), for this you will need an SKAO AD Azure user account. Documentation related to BinderHub can be found [here](https://developer.skao.int/en/latest/tools/binderhub.html)
2. Enter the gitlab repo URL into the URL field. The most recent versions of the software should be available in the `main` branch of this repo, as such no other entries are required.
3. Click "Launch".
4. BinderHub will then create an appropriate server for you to run an instance of the SKAO software suite.
5. From this point please follow the instructions given in the walkthrough notebooks Documentation, linked to in the above list of template Notebooks.

---

## For Developers
### Quickstart
First, clone this repository with:
```
git clone --recurse-submodules git@gitlab.com:ska-telescope/ska-jupyter-scripting.git
```

To refresh the GitLab Submodule, execute below commands:
```
git submodule update --recursive --remote
git submodule update --init --recursive
```

Install all dependencies using Poetry:
```
> poetry install
```

Go to the poetry shell, the poetry virtual environment:
```
> poetry shell
```

To update the poetry.lock file, use command:
```
> poetry update
```

Build a new Docker image for the CDM,run:
```
make oci-build
```

Execute the unit tests and lint the project with:
```
make python-test && make python-lint
```

Format the Python code:
```
make python-format
```

### Release a new version
See SKAO instructions [here](https://developer.skao.int/en/latest/tools/software-package-release-procedure.html#software-package-release-procedure)

### Setting up environment variable's
In order to execute Jupyter notebook on Low imaging scenario we need to set envirnment
variable at beginning as below and that already set in Low Jupyter notebook, It will segregate state model execution flow between Low and mid imaging scenarios.

```
import os
os.environ["SKA_TELESCOPE"] ="SKA-low"
```