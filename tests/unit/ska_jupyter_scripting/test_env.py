import json
import os

import pytest

from ska_jupyter_scripting.helpers.env import (
    Env,
    set_cluster,
    set_oda_url,
    smoke_test_cluster,
)

env = Env(
    namespace="",
    telescope="",
    database_name="",
    job_id="",
    SDP_CONFIG_HOST="",
    ingress_name="",
)


# def test_set_cluster():
#     set_cluster(
#         namespace="foo",
#         database_name="bar",
#         telescope=None,
#         helm_release="test",
#     )

#     assert env["database_name"] == "bar"
#     assert env["namespace"] == "foo"
